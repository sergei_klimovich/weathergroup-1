//
//  CityTableViewCell.swift
//  WeatherGroup-1
//
//  Created by Администратор on 20/06/2019.
//  Copyright © 2019 Sergey Klimovich. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    @IBOutlet weak var cityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

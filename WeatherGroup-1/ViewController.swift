//
//  ViewController.swift
//  WeatherGroup-1
//
//  Created by Администратор on 20/06/2019.
//  Copyright © 2019 Sergey Klimovich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    
    var city = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cityLabel.text = city
    }


}

